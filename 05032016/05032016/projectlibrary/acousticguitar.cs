﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05032016.projectlibrary
{
    class acousticguitar:guitar
    {
        private string _backbody;

        public string backbody 
        {
            get
            {
                return _backbody;
            }
            set 
            {
                if (value != null)
                    _backbody = value;
            }
        }

        public string typeofguitar
        {
            get
            {
                return "Акустика";
            }
        }

       
        public override string ToString()
        {
            return string.Format("Тип гитары:" + typeofguitar + "\n" + "Производитель:" + manufacturer + "\n" + "Название модели:" + name + "\n" + "Количество струн:" + numberofstrings + "\n" + "Дека:" + body + "\n" + "Гриф:" + neck + "\n" + "Задняя дека:" + backbody);
        }
    }
}
