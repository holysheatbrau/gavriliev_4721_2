﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05032016.projectlibrary
{
    class electroguitar:guitar
    {
        private string _pickup = "";
        private string _bridge = "";
   

        public string pickup
        {
            get
            {
                return _pickup;
            }
            set
            {
                if (value != null)
                    _pickup = value;
            }
        }

        public string typeofguitar
        {
            get
            {
                return "Электрогитара";
            }
        }

     
         public string bridge
         {
             get
             {
              return _bridge;
             }
             set
             {
              if (value != null)
              _bridge = value;
             }
         }
         public override string ToString()
         {
             return string.Format("Тип гитары:" + typeofguitar + "\n" + "Производитель:" + manufacturer + "\n" + "Название модели:" + name + "\n" + "Количество струн:" + numberofstrings + "\n" + "Дека:" + body + "\n" + "Гриф:" + neck + "\n" + "Звукосниматели:" + pickup + "\n" + "Струнодержатель:" + bridge);
         }
        
    }
}
